#-----------------------
# VARIAVEIS OBRIGATORIAS
#-----------------------

variable "vpc_cidr_block" {
  description = "Endereço de rede para VPC"
}

variable "subnet_cidr_blocks" {
  description = "Lista com os blocos de endereços de rede para as subnets"
  type        = "list"
}

#--------------------
# VARIAVEIS OPCIONAIS
#--------------------
variable "vpc_name" {
  description = "Nome para idnetificação da VPC"
  default     = "vpc"
}

variable "enable_dns_support" {
  description = "Habilitar/Desabilitar o suporte a DNS na VPC"
  default     = "true"
}

variable "enable_dns_hostnames" {
  description = "Habilitar/Desabilitar o suporte a Hostnames do DNS na VPC"
  default     = "false"
}

variable "tags" {
  description = "Tags para os recursos do módulo VPC"
  default     = {}
}
