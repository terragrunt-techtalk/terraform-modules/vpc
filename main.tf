data "aws_availability_zones" "all" {}

resource "aws_vpc" "main" {
  cidr_block       = "${var.vpc_cidr_block}"
  instance_tenancy = "default"

  enable_dns_support   = "${var.enable_dns_support}"
  enable_dns_hostnames = "${var.enable_dns_hostnames}"

  tags = "${
    merge(
      map("Name",var.vpc_name),
      var.tags
    )
  }"
}

resource "aws_subnet" "public" {
  vpc_id                  = "${aws_vpc.main.id}"
  count                   = "${length(var.subnet_cidr_blocks)}"
  cidr_block              = "${element(var.subnet_cidr_blocks, count.index)}"
  availability_zone       = "${element(data.aws_availability_zones.all.names, count.index)}"
  map_public_ip_on_launch = true

  tags = "${
    merge(
      map(
        "Name","${var.vpc_name}-pub-${count.index}",
        "Type", "public"
        ),
      var.tags
    )
  }"
}

resource "aws_internet_gateway" "main" {
  vpc_id = "${aws_vpc.main.id}"

  tags = "${var.tags}"
}

resource "aws_route_table" "public" {
  vpc_id = "${aws_vpc.main.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.main.id}"
  }

  tags = "${
    merge(
      var.tags,
      map(
        "Name", "${var.vpc_name}-public-rt"
      )
    )
  }"
}

resource "aws_route_table_association" "public" {
  count          = "${length(var.subnet_cidr_blocks)}"
  subnet_id      = "${element(aws_subnet.public.*.id, count.index)}"
  route_table_id = "${aws_route_table.public.id}"
}
